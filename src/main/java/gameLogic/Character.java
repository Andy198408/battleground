package gameLogic;

import utility.KeyboardUtility;

import java.util.ArrayList;

public abstract class Character {

    static int MAXHEALTH = 100;

    protected int shieldHealth;
    protected String name, specialAbilityName;
    protected int health;

    protected ArrayList<Integer> dmgReport;

    public Character() {
    }

    public Character(String name, int health, int shieldHealth) {
        this.name = name;
        this.health = health;
        this.shieldHealth = shieldHealth;

        this.dmgReport = new ArrayList<>();
    }

    public Character(Character character) {
        this.shieldHealth = character.getShieldHealth();
        this.name = character.getName();
        this.health = character.getHealth();
    }

    public int attack() {
        int attack = KeyboardUtility.getRandomInt(getHealth() + 10);
        return attack;
    }

    public int defend() {
        int defense = 0;
        if (shieldHealth > 0) {
            defense = KeyboardUtility.getRandomInt(getHealth());
        }
        return defense;
    }

    public boolean specialAbility() {
        throw new UnsupportedOperationException();
    }

    public int getShieldHealth() {
        return shieldHealth;
    }

    public void setShieldHealth(int shieldHealth) {
        this.shieldHealth = shieldHealth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public String getSpecialAbilityName() {
        return specialAbilityName;
    }

    public void setSpecialAbilityName(String specialAbilityName) {
        this.specialAbilityName = specialAbilityName;
    }

    public void addDmg(int dmg) {
        dmgReport.add(dmg);
    }

}
