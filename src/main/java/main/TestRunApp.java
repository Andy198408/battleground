package main;

import gameLogic.PlayerVsPlayer;
import utility.KeyboardUtility;

public class TestRunApp {
    public static void main(String[] args) {
        do {
            new PlayerVsPlayer().run();
        } while (KeyboardUtility.getBoolean("\nDo you want to play again?"));
    }
}
