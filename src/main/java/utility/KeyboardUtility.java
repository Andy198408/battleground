package utility;

import java.util.Random;
import java.util.Scanner;

public final class KeyboardUtility {

    static Random rnd = new Random();
    static Scanner in = new Scanner(System.in);

    public static int getInt(String msg) {
        System.out.print(msg);
        int value = -1;
        try {
            value = in.nextInt();
        } catch (Exception e) {
            System.out.println(e + " unable to process your input");
        } finally {
            in.nextLine();
        }
        return value;
    }

    public static double getDouble(String msg) {
        System.out.print(msg);
        double value = -1;
        try {
            value = in.nextInt();
        } catch (Exception e) {
            System.out.println(e + " unable to process your input");
        } finally {
            in.nextLine();
        }
        return value;
    }

    /**
     * Ask the user for a String(repeat untill input is correct).
     *
     * @param question          the question to ask(print to) the user.
     * @param minimumCharacters the minimum length of String to return.
     * @return the user input: string.
     */
    public static String askUserString(String question, int minimumCharacters) {
        if (minimumCharacters <= 0) {
            System.out.print(question);
            return in.nextLine();
        } else {
            String input;// = null;
            do {
                System.out.print(question);
                input = in.nextLine();
                if (input.length() < minimumCharacters)
                    System.err.format("Error: Input must be at least %d character%s.\n", minimumCharacters, minimumCharacters > 1 ? "s" : "");
            } while (input.length() < minimumCharacters);
            return input;
        }
    }

    /**
     * Ask the user for a String(repeat untill input is correct).
     *
     * @param question the question to ask(print to) the user.
     * @return the user input: string.
     */
    public static String askUserString(String question) {
        return askUserString(question, 0);
    }

    public static boolean getBoolean(String msg) {
        System.out.println(msg);

        System.out.println("Valid answer is y/n");
        String answer = in.nextLine().toLowerCase();

        if (answer.startsWith("y")) {
            return true;
        } else {
            return false;
        }
    }

    public static int getRandomInt(int bound) {
        return rnd.nextInt(bound);
    }

    public static void hardReset() {
        in.close();
        in = new Scanner(System.in);
    }

    public static void close() {
        in.close();
    }
}
