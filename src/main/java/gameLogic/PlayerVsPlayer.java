package gameLogic;

import utility.KeyboardUtility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerVsPlayer implements Runnable {

    private Character player1;
    private Character player2;
    private Character attacker;
    private Character defender;

    private boolean fight = true;

    private int attack;

    private HashMap<String, ArrayList<Integer>> battleReport;

    public PlayerVsPlayer() {
        battleReport = new HashMap<>();
    }

    @Override
    public void run() {

        System.out.println("\nWelcome to the battleground where buttonsmashers come to die!");

        player1 = new Player(KeyboardUtility.askUserString("\nPlayer 1 enter name: "), 100, 100);
        player2 = new Player(KeyboardUtility.askUserString("\nPlayer 2 enter name: "), 100, 100);
        battleReport.put(player1.getName(), player1.dmgReport);
        battleReport.put(player2.getName(), player2.dmgReport);

        System.out.println("\nThe player that can guess closed to a random number between 0 and 10 may start:\n ");

//      Setting the first attacker
        setFirstAttacker(player1, player2);

//      Starting battleSequence
        while (fight) {

            attack = attacker.attack();
            System.out.println("\n" + attacker.getName() + " you engage " + defender.getName() + " and attack for " + attack + " damage.");

            System.out.println(defender.getName() + " you see " + attacker.getName() + " attacking you! ");

//          Showing choice menu en executing it.
            executeStrategy(strategyMenu());

//          After battle options
            if (defender.getHealth() > 0) {
                System.out.println(defender.getName() + " do you want to use your special ability? : " + defender.getSpecialAbilityName());
                if (KeyboardUtility.getBoolean("")) {
                    if (defender.specialAbility()) {
                        System.out.println("You were able to heal yourself back to " + defender.getHealth());
                    } else {
                        System.out.println("Unfortunately you have used your ability to much!");
                    }
                }
            }

//          Switching attacker position
            if (defender.getHealth() > 0) {
                fight = switchAttacker();
            }
        }

        showBattleReport();
    }

    private void useShield(int attack) {
        int defend = defender.defend();
        int prevent = defending(attack, defend);
        System.out.println("You quickly raise your shield! You prevent " + prevent + " damage!");

//          Calculating battle damages
        calculateShieldDmg(prevent);
        calculateBattleDmg(attack, prevent);
        System.out.println(defender.getName() + " has " + defender.getHealth() + " health left.");

    }

    private void executeStrategy(int choice) {
        switch (choice) {
            case 0:
                fight = false;
                System.out.println(defender.getName() + " Doesn't feel like dropping today and runs away!");
                defender.setHealth(-1);
                break;
            case 1:
                useShield(attack);
                break;

            default:
                System.out.println(defender.getName() + " dropped the ball and made a wrong move, and now dies without effort!");
                defender.setHealth(-1);
                fight = false;
                break;
        }
    }

    private int strategyMenu() {
        List<String> options = Arrays.asList("Run away", "Use shield");
        ArrayList<String> optionMenu = new ArrayList<>(options);
        optionMenu.forEach((o) -> System.out.println("\t" + optionMenu.indexOf(o) + " : " + o));
        return KeyboardUtility.getInt("Choose an option! : ");
    }

    private void showBattleReport() {
        System.out.println("\nBattlereport: " + player1.getName());
        int totalDmgPlayer1 = battleReport.get(player1.getName()).stream()
                .collect(Collectors.summingInt(Integer::intValue));
        battleReport.get(player1.getName()).forEach(System.out::println);
        System.out.println("Total damage done: " + totalDmgPlayer1);

        System.out.println("\nBattlereport: " + player2.getName());
        int totalDmgPlayer2 = battleReport.get(player2.getName()).stream()
                .collect(Collectors.summingInt(Integer::intValue));
        battleReport.get(player2.getName()).forEach(System.out::println);
        System.out.println("Total damage done: " + totalDmgPlayer2);


    }

    private boolean switchAttacker() {
        if (defender.getHealth() > 0) {
            if (KeyboardUtility.getBoolean("Do you want to attack your opponent?")) {
                Character temp = attacker;
                attacker = defender;
                defender = temp;
                return true;
            } else {
                System.out.println("You run away!");
                return false;
            }
        } else {
            System.out.println("You have died heroically in battle and " +
                    "will drink ale from curved horns along with your ancestors. " +
                    "The valkyrja have come to take you! ");
            return false;
        }
    }

    private void calculateBattleDmg(int attack, int prevent) {
        int finalDmg = attack - prevent;
        finalDmg = (Math.max(finalDmg, 0));
        defender.setHealth(defender.getHealth() - finalDmg);
        attacker.addDmg(finalDmg);
    }

    private void calculateShieldDmg(int prevent) {
        defender.setShieldHealth(defender.getShieldHealth() - prevent);
    }

    private int defending(int attack, int defend) {
        int temp = attack;
        attack = attack - defend;
        if (attack < 0) attack = 0;
        return temp - attack;
    }

    private void setFirstAttacker(Character player1, Character player2) {

        int rndNumber = KeyboardUtility.getRandomInt(10);
//        System.out.println(rndNumber);

        System.out.println(player1.getName());
        int playerGuess1 = KeyboardUtility.getInt("Guess a number: ");
        int dif1 = Math.abs(rndNumber - playerGuess1);
        System.out.println(player2.getName());
        int playerGuess2 = KeyboardUtility.getInt("Guess a number: ");
        int dif2 = Math.abs(rndNumber - playerGuess2);

        if (dif1 < dif2) {
            System.out.println(player1.getName() + " You win, let's see if your luck continues!");
            attacker = player1;
            defender = player2;
        } else {
            System.out.println(player2.getName() + " You win, let's see if your luck continues!");
            attacker = player2;
            defender = player1;
        }
    }
}